<?php

namespace Drupal\varnishvanish;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;

/**
 * Class FormVarnishVarnishForm.
 */
class FormVarnishVanishForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_varnishvanish_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit_button'] = [
      '#type' => 'submit',
      '#value' => t('Clear Vanish Cache!'),
      '#submit' => [
        'form_varnishvanish_form_submit',
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    varnishvanish_clear_all();
  }

}
