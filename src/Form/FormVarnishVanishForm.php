<?php

/**
 * @file
 * Contains \Drupal\varnishvanish\Form\FormVarnishVanishForm.
 */

namespace Drupal\varnishvanish\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class FormVarnishVanishForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_varnishvanish_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit_button'] = [
      '#type' => 'submit',
      '#value' => t('Clear Vanish Cache!'),
      '#submit' => [
        'form_varnishvanish_form_submit'
        ],
    ];
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    varnishvanish_clear_all();
  }

}
?>
